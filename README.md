# Snowflak, Lit-Element, Typescript, and Bulma 

## What is this?

A super minimal app that uses both [snowpack](https://www.snowpack.dev/), [bulma](https://bulma.io/), [lit-element](https://lit-element.polymer-project.org/),and [typescript](https://www.typescriptlang.org/) to bring some joy into our locked-down lives.

## Why?

* Snowpack - a super fast front-end build tool, capable of generating modern "bare imports" which have performance advantages with modern web servers.
* Lit-Element is long-time favorite for the creation of componentized applications using web standards.
* Typescript - better than Javascript. 
* Bulma - an experiment to show the inclusion of an external 3rd party CSS framework.
* Canvas Confetti - we're using this to demonstrate the use of a 3rd party module.

## Cloning this template

```bash
git clone git@gitlab.com:dazraf/snowpack-lit-element-typescript-bulma.git <projec-directory>
cd <project-directory>
git remote remove origin
```

## How do I run it?

```bash
npm install
npm start
```

## How do I build for production?

```bash
npm run build
```
