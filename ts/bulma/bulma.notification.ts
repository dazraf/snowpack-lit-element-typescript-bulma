import { html } from "lit-html";
import { BulmaComponent } from "./bulma.component";
import { bulmaCss} from './bulma-style';

export class BulmaNotification extends BulmaComponent {
  static styles = bulmaCss;

  render() {
    return html`
      <div class="notification ${super.getRenderClasses()}">
      <span class="delete" @click="${this.onClose}"></span>
      <slot></slot>
    </div>
    `;
  }
  private onClose() {
    this.dispatchEvent(new CustomEvent('onclose', {
      detail: { message: 'notification closed'},
      bubbles: true,
      composed: true
    }))
  }
}