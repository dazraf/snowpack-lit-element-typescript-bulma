import { LitElement, property } from "lit-element";
import {classMap} from 'lit-html/directives/class-map';

export class BulmaComponent extends LitElement {
  @property({type: Boolean, reflect: true}) content = false;
  @property({type: Boolean, reflect: true}) block = false;
  @property({type: Boolean, reflect: true}) isPrimary = false;
  @property({type: Boolean, reflect: true}) isLink = false;
  @property({type: Boolean, reflect: true}) isInfo = false;
  @property({type: Boolean, reflect: true}) isSuccess = false;
  @property({type: Boolean, reflect: true}) isWarning = false;
  @property({type: Boolean, reflect: true}) isDanger = false;
  @property({type: Boolean, reflect: true}) isSmall = false;
  @property({type: Boolean, reflect: true}) isMedium = false;
  @property({type: Boolean, reflect: true}) isLarge = false;
  @property({type: Boolean, reflect: true}) isOutlined = false;
  @property({type: Boolean, reflect: true}) isLoading = false;
  @property({type: Boolean, reflect: true}) isDisabled = false;

  
  protected getRenderClasses() {
    return classMap(this.getClassMap());
  }

  protected getClassMap(): any {
    return {
      'content': this.content,
      'block': this.block,
      'is-primary': this.isPrimary,
      'is-info': this.isInfo,
      'is-success': this.isSuccess,
      'is-warning': this.isWarning,
      'is-link': this.isLink,
      'is-danger': this.isDanger,
      'is-small': this.isSmall,
      'is-medium': this.isMedium,
      'is-large': this.isLarge,
      'is-outlined': this.isOutlined,
      'is-loading': this.isLoading,
      'is-disabled': this.isDisabled
    }
  }
}