import { css } from 'lit-element';
import { BulmaBox } from './bulma.box';
import { BulmaButton } from './bulma.button';
import { BulmaContent } from './bulma.content';
import { BulmaDelete } from './bulma.delete';
import { BulmaIcon } from './bulma.icon';
import { BulmaNotification } from './bulma.notification';

customElements.define('x-button', BulmaButton);
customElements.define('x-content', BulmaContent);
customElements.define('x-box', BulmaBox);
customElements.define('x-delete', BulmaDelete);
customElements.define('x-icon', BulmaIcon);
customElements.define('x-notification', BulmaNotification);
