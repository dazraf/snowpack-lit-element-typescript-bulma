import { html } from "lit-html";
import { BulmaComponent } from "./bulma.component";
import { bulmaCss} from './bulma-style';

export class BulmaContent extends BulmaComponent {
  static styles = bulmaCss;
  constructor() {
    super();
    this.content = true;
  }
  render() {
    return html`
      <main class="button ${super.getRenderClasses()}"><slot></slot></main>
    `;
  }
}