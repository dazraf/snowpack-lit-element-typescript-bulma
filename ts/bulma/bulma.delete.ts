import { html } from "lit-html";
import { BulmaComponent } from "./bulma.component";
import { bulmaCss} from './bulma-style';


export class BulmaDelete extends BulmaComponent {
  static get styles() {
    return [
      bulmaCss,
    ]
  }

  render() {
    return html`
      <span class="delete" @click="${this.onclick}"></span>
    `;
  }
}