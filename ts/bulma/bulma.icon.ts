import { html } from "lit-html";
import { BulmaComponent } from "./bulma.component";
import { bulmaCss } from './bulma-style';
import { property } from "lit-element";
import { fontAwesomeCss } from '../fa';

export class BulmaIcon extends BulmaComponent {
  static get styles() {
    return [
      bulmaCss,
      fontAwesomeCss
    ]
  }
  @property({ type: String, reflect: true }) icon = "fa-home";

  constructor() {
    super();
    // const fontEl = document.createElement('link');
    // fontEl.rel = 'stylesheet';
    // fontEl.href = 'https://use.fontawesome.com/releases/v5.0.13/css/all.css';
    // document.head.appendChild(fontEl);
  }

  render() {
    if (this.children != null && this.children.length > 0) {
      return this.iconWithText();
    } else {
      return this.pureIcon();
    }
  }

  private pureIcon() {
    return html`
      <span class="icon ${super.getRenderClasses()}">
        <span class="fas ${this.icon}"></span>
      </span>
  `;
  }

  private iconWithText() {
    return html`
    <span class="icon-text ${super.getRenderClasses()}">
      <span class="icon">
        <span class="fas ${this.icon}"></span>
      </span>
      <slot></slot>
    </span>
  `;
  }
}