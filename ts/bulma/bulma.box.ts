import { html } from "lit-html";
import { BulmaComponent } from "./bulma.component";
import { bulmaCss} from './bulma-style';


export class BulmaBox extends BulmaComponent {
  static styles = bulmaCss;

  render() {
    return html`
      <div class="box ${super.getRenderClasses()}"><slot></slot></div>
    `;
  }
}