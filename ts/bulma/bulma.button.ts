import { html } from "lit-html";
import { BulmaComponent } from "./bulma.component";
import { bulmaCss} from './bulma-style';


export class BulmaButton extends BulmaComponent {
  static styles = bulmaCss;

  render() {
    return html`
      <button class="button ${super.getRenderClasses()}" @click="${this.onclick}"><slot></slot></button>
    `;
  }
}