import { LitElement, html, css } from "lit-element";
import confetti from 'canvas-confetti';
import { bulmaCss } from './bulma/bulma-style';
import './bulma';
export class App extends LitElement {
    static get styles() {
        return bulmaCss;
    }

    render() {
        return html`
        <main class="content">
            <!-- using Bulma classes -->
            <x-box>
                <h1>Welcome to Snowpack!</h1>
                <x-button isPrimary @click="${this.ooh}">
                    <x-icon isLarge icon="fa-home fa-lg"><span>Press Me!</span></x-icon>
                </x-button>
                <x-delete @click="${this.ondelete}"></x-delete>
                <div style="width: 300px">
                    <x-notification @onclose="${this.onCloseNotification}">A simple notification</x-notification>
                </div>
            </x-box>
        </main>`
    }

    ooh() {
        confetti.create(document.getElementById('canvas') as HTMLCanvasElement, {
            resize: true,
            useWorker: true,
        })({ particleCount: 200, spread: 200 });
    }

    ondelete() {
        alert("Deleted!")
    }

    onCloseNotification() {
        alert("On close notification");
    }
};